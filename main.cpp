#include <iostream>

#include "xyz-adapter-jsclass.h"

int main()
{
    auto rt = ::JS_NewRuntime();
    auto ctx = ::JS_NewContext(rt);
    {
        XyzAdapterJsClass xyz(ctx);
        xyz.defineGlobalPropery();

        std::string scriptText{
            "let xyz = new Xyz();"
            "xyz.foo();"
        };

        JSValue r = JS_Eval(ctx, scriptText.c_str(), scriptText.size(), "<test>", 0);
        if ( JS_IsException(r) ) {
            JSValue error = JS_GetException( ctx );
            auto errorStr = JS_ToCString(ctx, error);

            std::cout << errorStr << std::endl;

            JS_FreeCString( ctx, errorStr );
            JS_FreeValue( ctx, error );
        }

        JS_FreeValue( ctx, r );
    }
    ::JS_FreeContext(ctx);
    ::JS_FreeRuntime(rt);

    return 0;
}
