#ifndef ABSTRACTADAPTERJSCLASS_H
#define ABSTRACTADAPTERJSCLASS_H

#include <map>
#include <quickjs.h>

class AbstractAdapterJsClass
{
public:
    AbstractAdapterJsClass( JSContext* ctx, const char* className);

    virtual ~AbstractAdapterJsClass();

    const char* jsClassName() const;

    void defineGlobalPropery() const;

    static JSValue jsCallFuncProxy(JSContext *ctx, JSValueConst func_obj,
                                JSValueConst this_val, int argc, JSValueConst *argv, int flags);

protected:
    virtual JSValue jsObjectCtor(int argc, JSValueConst* argv ) = 0;
    virtual JSValue jsObjectCallFuncProxy(int argc, JSValueConst* argv ) = 0;

private:
    JSContext* m_ctx;
    JSClassID m_jsClassID;
    const JSClassDef m_jsClassDef;
    JSValue m_prototype;
    JSValue m_jsFuncObject;

    static std::map<void*, AbstractAdapterJsClass*> m_adapters;
};

#endif // ABSTRACTADAPTERJSCLASS_H
