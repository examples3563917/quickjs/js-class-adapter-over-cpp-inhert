#include "xyz-adapter-jsclass.h"

#include <iostream>

XyzAdapterJsClass::XyzAdapterJsClass(JSContext* ctx): AbstractAdapterJsClass(ctx, "Xyz"){}

JSValue XyzAdapterJsClass::jsObjectCtor(int argc, JSValue* argv)
{
    std::cout << __FUNCTION__ << std::endl;
    return JS_UNDEFINED;
}

JSValue XyzAdapterJsClass::jsObjectCallFuncProxy(int argc, JSValue* argv)
{
    std::cout << __FUNCTION__ << std::endl;
    return JS_UNDEFINED;
}
