#include "abstract-adapter-jsclass.h"

#include <cassert>

std::map<void*, AbstractAdapterJsClass*> AbstractAdapterJsClass::m_adapters;

AbstractAdapterJsClass::AbstractAdapterJsClass(JSContext* ctx, const char* className)
        : m_ctx(ctx)
        , m_jsClassID{0}//required default 0 for autoincrement
        , m_jsClassDef{ .class_name = className, .call = jsCallFuncProxy }
    {
        ::JS_NewClassID( &m_jsClassID );

        auto rt = ::JS_GetRuntime(m_ctx);
        auto r = JS_NewClass(rt, m_jsClassID, &m_jsClassDef);
        assert( r == 0 );

        m_prototype = ::JS_NewObject(m_ctx);
        ::JS_SetClassProto(m_ctx, m_jsClassID, m_prototype);

        //copy from global object Function.prototype
        JSValue globalObject = ::JS_GetGlobalObject( m_ctx );
        JSValue functionConstructor = JS_GetPropertyStr( m_ctx, globalObject, "Function" );
        assert( !::JS_IsUndefined( functionConstructor ) );
        JSValue functionPrototype = JS_GetPropertyStr( m_ctx, functionConstructor, "prototype" );
        assert( !::JS_IsUndefined( functionPrototype ) );

        //создаем на основе прототипа функциональный обьект, заготовка для конструктора
        m_jsFuncObject = JS_NewObjectProtoClass( m_ctx, functionPrototype, m_jsClassID );

        //создаем произвольную функцию и назначаем ее прототипу
        // auto customFunc = ::JS_NewObjectProto(m_ctx, functionPrototype);
        // assert( ::JS_IsFunction( m_ctx, customFunc) );
        //r = ::JS_DefinePropertyValueStr( m_ctx, m_prototype, "hello", customFunc, JS_PROP_C_W_E);
        //r = JS_SetPropertyStr(m_ctx, m_prototype, "hello", customFunc);
        //assert( r == 0 );

        JS_FreeValue(m_ctx, functionPrototype);
        JS_FreeValue(m_ctx, functionConstructor);
        JS_FreeValue(m_ctx, globalObject);


        ::JS_SetConstructorBit( m_ctx, m_jsFuncObject, true );

        assert( ::JS_IsFunction( m_ctx, m_jsFuncObject) );

        m_adapters.insert( {JS_VALUE_GET_PTR(m_jsFuncObject), this} );
    }

AbstractAdapterJsClass::~AbstractAdapterJsClass()
    {
        ::JS_FreeValue(m_ctx, m_jsFuncObject);
        //TODO free prototype?
    }

const char* AbstractAdapterJsClass::jsClassName() const
    {
        return m_jsClassDef.class_name;
    }

void AbstractAdapterJsClass::defineGlobalPropery() const
    {
      JSValue globalObject = ::JS_GetGlobalObject(m_ctx);
      ::JS_SetPropertyStr(m_ctx, globalObject, jsClassName(), m_jsFuncObject);
      ::JS_FreeValue(m_ctx, globalObject);
    }

JSValue AbstractAdapterJsClass::jsCallFuncProxy(JSContext* ctx, JSValueConst func_obj, JSValueConst this_val, int argc, JSValueConst* argv, int flags)
    {
        auto adapter = m_adapters.at( JS_VALUE_GET_PTR(func_obj) );
        if (adapter) {
            if((flags & JS_CALL_FLAG_CONSTRUCTOR) != 0) {
                return adapter->jsObjectCtor(argc, argv);
            } else {
                return adapter->jsObjectCallFuncProxy(argc, argv);
            }

            //Здесь все остальное
        }

        return JS_EXCEPTION;
}
