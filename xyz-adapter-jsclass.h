#ifndef XYZADAPTERJSCLASS_H
#define XYZADAPTERJSCLASS_H

#include "abstract-adapter-jsclass.h"

class XyzAdapterJsClass : public  AbstractAdapterJsClass
{
public:
    XyzAdapterJsClass(JSContext* ctx);;

// AbstractAdapterJsClass interface
protected:
JSValue jsObjectCtor(int argc, JSValue* argv) override;
JSValue jsObjectCallFuncProxy(int argc, JSValue* argv) override;

};

#endif // XYZADAPTERJSCLASS_H
